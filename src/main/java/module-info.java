module fr.l.mycontrolsfx {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;


    opens fr.l.mycontrolsfx to javafx.fxml;
    exports fr.l.mycontrolsfx;
    exports fr.l.mycontrolsfx.controlledtextfield;
    opens fr.l.mycontrolsfx.controlledtextfield to javafx.fxml;
}