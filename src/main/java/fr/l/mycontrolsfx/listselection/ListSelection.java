package fr.l.mycontrolsfx.listselection;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.List;

public class ListSelection<T> extends BorderPane {
    @FXML
    private ListView<T> listViewAvailable;
    @FXML
    private ListView<T> listViewSelected;
    @FXML
    private AnchorPane anchorPaneFilter;
    @FXML
    private TextField textFieldFilter;
    @FXML
    private Button buttonSelect;
    @FXML
    private Button buttonSelectAll;
    @FXML
    private Button buttonUnselect;
    @FXML
    private Button buttonUnselectAll;
    private final ListSelectionBean<T> bean;
    private BorderPane borderPane;


    public ListSelection() {
        this.bean = new ListSelectionBean<>();
        loadComposant();
        filtered(false);
        listViewAvailable.setItems(bean.getAvailableFilter());
        listViewSelected.setItems(bean.getSelectedFilter());
        setItemClick();
        /*setDragStart();
        setDragOver();
        setDragDropped();*/
        textFieldFilter.textProperty().addListener(((observable, oldValue, newValue) -> bean.filter(newValue)));
    }

    public void setLists(List<T> available, List<T> selected) {
        bean.setLists(available,selected);
    }

    private void setItemClick() {
        listViewAvailable.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2)
                selectOne();
            if (event.getClickCount() == 3)
                selectAll();
        });
        listViewSelected.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2)
                unselectOne();
            if (event.getClickCount() == 3)
                unselectAll();
        });
    }

    public void unselectOne() {
        bean.unselect(listViewSelected.getSelectionModel().getSelectedItems());
    }

    public void selectOne() {
        bean.select(listViewAvailable.getSelectionModel().getSelectedItems());
    }

    public void selectAll() {
        bean.selectAll();
    }

    public void unselectAll() {
        bean.unselectAll();
    }
    private void filtered(boolean b) {
    }

    private void loadComposant() {
        FXMLLoader myFxmlLoader = new FXMLLoader(getClass().getResource("ListSelection.fxml"));
        myFxmlLoader.setRoot(this);
        myFxmlLoader.setController(this);
        try {
            borderPane = myFxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
