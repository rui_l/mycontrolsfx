package fr.l.mycontrolsfx.listselection;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

import java.util.List;
import java.util.Objects;

public class ListSelectionBean<T> {
    private final ObservableList<T> availableList;
    private final ObservableList<T> selectedList;
    private final FilteredList<T> availableFilteredList;
    private final FilteredList<T> selectedFilteredList;


    public ListSelectionBean() {
        this.availableList = FXCollections.observableArrayList();
        this.selectedList = FXCollections.observableArrayList();
        this.availableFilteredList = new FilteredList<>(this.availableList);
        this.selectedFilteredList = new FilteredList<>(this.selectedList);
    }

    public void setLists(List<T> disponible, List<T> selection) {
        for (T element : disponible) {
            if (!selection.contains(element))
                this.availableList.add(element);
        }
        this.selectedList.addAll(selection);
    }
    public void filter(String newValue) {
        availableFilteredList.setPredicate(object->object.toString().toLowerCase().contains(newValue.toLowerCase()));
        selectedFilteredList.setPredicate(object->object.toString().toLowerCase().contains(newValue.toLowerCase()));
    }

    public ObservableList<T> getAvailableFilter() {
        return availableFilteredList;
    }

    public ObservableList<T> getSelectedFilter() {
        return selectedFilteredList;
    }

    public void select(ObservableList<T> list) {
        selectedList.addAll(list);
        availableList.removeAll(list);
    }

    public void unselect(ObservableList<T> list) {
        availableList.addAll(list);
        selectedList.removeAll(list);
    }

    public void selectAll() {
        selectedList.addAll(availableFilteredList);
        availableList.removeAll(availableFilteredList);
    }

    public void unselectAll() {
        availableList.addAll(selectedFilteredList);
        selectedList.removeAll(selectedFilteredList);
    }

}
