package fr.l.mycontrolsfx;

import fr.l.mycontrolsfx.listselection.ListSelection;
import javafx.application.Application;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

public class TestListSelection extends Application {
    List<String> colors = Arrays.asList("Aqua", "Beige", "Black", "Blue", "BlueViolet", "Brown", "Chartreuse", "Coral", "Cyan", "DarkBlue", "DarkGreen", "DarkRed", "Gray", "Green", "Red", "White", "Yellow");
    List<String> selectedColor = Arrays.asList("Orange", "Pink");
    @Override
    public void start(Stage stage) throws Exception {
        ListSelection<String> listSelection1 = new ListSelection<>();
        listSelection1.setLists(colors,selectedColor);

        AnchorPane rootLayout = new AnchorPane();

    }
}
