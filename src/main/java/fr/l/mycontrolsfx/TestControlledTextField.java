package fr.l.mycontrolsfx;

import fr.l.mycontrolsfx.controlledtextfield.ControlledTextField;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TestControlledTextField extends Application {

    ControlledTextField controlledTextFieldTelephone;
    ControlledTextField controlledTextFieldCodePostal;
    @Override
    public void start(Stage stage) throws Exception {
        controlledTextFieldTelephone = new ControlledTextField();
        controlledTextFieldTelephone.setParameters("OK", "Format incorrect!", "(0[1-9]|\\+33[\\s.]?[1-9])([\\s.]*[0-9]{2}){4}$");
        controlledTextFieldTelephone.setLabelText("Telephone");
        //controlledTextFieldTelephone.setHorizontal(false);
        controlledTextFieldTelephone.setLabelTextWidth(80);

        controlledTextFieldCodePostal = new ControlledTextField();
        controlledTextFieldCodePostal.setParameters("OK", "Format incorrect!", "^(2[0-9AB]|[013-9][0-9])[0-9]{3}");
        controlledTextFieldCodePostal.setLabelText("Code Postal");
        //controlledTextFieldCodePostal.setHorizontal(false);
        controlledTextFieldCodePostal.setLabelTextWidth(80);

        ControlledTextField controlledTextFieldMotDePasse = new ControlledTextField();
        controlledTextFieldMotDePasse.setParameters("OK", "Format incorrect!", "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&+-_=|€,.:;]).{8,15}$");
        controlledTextFieldMotDePasse.setLabelText("Mot de passe");
        //controlledTextFieldMotDePasse.setHorizontal(false);
        controlledTextFieldMotDePasse.setLabelTextWidth(80);

        VBox rootLayout = new VBox();
        rootLayout.getChildren().add(controlledTextFieldTelephone);
        rootLayout.getChildren().add(controlledTextFieldCodePostal);
        rootLayout.getChildren().add(controlledTextFieldMotDePasse);

        stage.setScene(new Scene(rootLayout));
        stage.setWidth(300);
        stage.setHeight(200);
        stage.show();


    }

    public static void main(String[] args) {
            launch(args);
    }

    @Override
    public void stop() throws Exception {
        System.out.printf("Telephone\t : %s \nCode postal\t : %s\n",controlledTextFieldTelephone.getTextOfTextField(),controlledTextFieldCodePostal.getTextOfTextField());
    }
}
