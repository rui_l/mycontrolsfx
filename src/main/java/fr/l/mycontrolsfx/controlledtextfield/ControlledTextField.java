package fr.l.mycontrolsfx.controlledtextfield;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

public class ControlledTextField extends BorderPane {
    @FXML
    private TextField textField;
    @FXML
    private Label labelText;
    @FXML
    private Label labelError;
    private boolean correct;
    private boolean horizontal = true;

    private BorderPane borderPane;

    public ControlledTextField() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ControlledTextField.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try{
            borderPane = fxmlLoader.load();
        } catch (IOException e)  {
            e.printStackTrace();
        }
        labelText.setText("");
        labelError.setText("Error");
    }

    public void setHorizontal(boolean bool) {
        BorderPane.setAlignment(labelText,Pos.CENTER_LEFT);
        BorderPane.setAlignment(labelError, Pos.CENTER_LEFT);
        if(bool) {
            borderPane.setTop(null);
            borderPane.setBottom(null);
            borderPane.setLeft(labelText);
            borderPane.setRight(labelError);
        } else {
            borderPane.setLeft(null);
            borderPane.setRight(null);
            borderPane.setTop(labelText);
            borderPane.setBottom(labelError);
        }
    }

    public void setLabelText(String label) {
        labelText.setText(label);
    }

    public void setParameters(String alertCorrectText, String alertIncorrectText, String regularExpression){
        matchExpression(alertCorrectText,alertIncorrectText,regularExpression,textField.getText());
        textField.textProperty().addListener((obs, oldText, newText)->
                matchExpression(alertCorrectText,alertIncorrectText,regularExpression,newText));
    }

    private void matchExpression(String alertCorrectText, String alertIncorrectText, String regularExpression, String text) {
        if (text.matches(regularExpression)) {
            labelError.setText(alertCorrectText);
            labelError.setTextFill(Color.GREEN);
            correct = true;
        } else {
            labelError.setText(alertIncorrectText);
            labelError.setTextFill(Color.RED);
            correct = false;
        }
    }

    public void setLabelTextWidth(int valueOfWidth) {
        labelText.setMinWidth(valueOfWidth);
    }

    public String getTextOfTextField() {
        if (correct)
            return textField.getText();
        return null;
    }


}