Some JavaFX components.

For example: controlled text field 

RegEx of a french telephone number format: (0[1-9]|\+33[\s.]?[1-9])([\s.]*[0-9]{2}){4}$

RegEx of a french postal code format: ^(2[0-9AB]|[013-9][0-9])[0-9]{3}

RegEx of a password: ^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&+-_=|€,.:;]).{8,15}$

![Controlled Text Field example](src/main/resources/images/ControlledTextFieldExample.PNG)